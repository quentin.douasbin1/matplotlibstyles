#Author: Quentin Douasbin, CTR, Stanford University, California, USA


mathtext.fontset : stix
font.family : STIXGeneral
font.size: 12

lines.linewidth: 1.
lines.markersize: 6.
lines.markeredgewidth: 1.

#patch.linewidth: 0.5
patch.facecolor: blue
patch.edgecolor: eeeeee
patch.antialiased: True

text.hinting_factor: 8

axes.edgecolor: 0
#axes.linewidth: 0.75
#axes.linewidth: 1.5
axes.facecolor: 0.985
#axes.facecolor: 0.99
axes.grid: True
axes.prop_cycle: cycler('color', [ '0.00', '348ABD', 'A60628', '0.50', '467821', 'D55E00', 'CC79A7', '56B4E9', '009E73', 'F0E442'])

#axes.spines.right : False
# axes.spines.left : True
# axes.spines.right : True
# axes.spines.top : True
# axes.spines.bottom : True
#axes.autolimit_mode : data

grid.color: 0.4 # grayscale form 0 to 1 (0 == black, 1 == white)
grid.linestyle: :
grid.linewidth: 0.45

legend.fancybox: False
legend.shadow: False
legend.edgecolor: black
legend.handlelength: 1.
legend.borderpad: 0.25

#figure.figsize: 8, 3.5
figure.figsize: 4.0, 3.5
figure.autolayout : True

image.cmap: viridis

savefig.dpi: 100
savefig.format: pdf
#savefig.format: png
#savefig.pad_inches: 0.1

markers.fillstyle: none

xtick.direction: in
ytick.direction: in

xtick.top : True
ytick.right : True

xtick.major.size: 5
#xtick.major.width: 1.5
xtick.minor.size: 3
#xtick.minor.width: 1.0

ytick.major.size: 5
#ytick.major.width: 1.5
ytick.minor.size: 3
#ytick.minor.width: 1.00

xtick.minor.visible: True
ytick.minor.visible: True

# Use scientific notation
axes.formatter.limits: -4, 4        ## use scientific notation if log10
                                      ## of the axis range is smaller than the
                                      ## first or larger than the second
# axes.formatter.use_locale: False    ## When True, format tick labels
                                      ## according to the user's locale.
                                      ## For example, use ',' as a decimal
                                      ## separator in the fr_FR locale.
 axes.formatter.use_mathtext: True   ## When True, use mathtext for scientific
                                      ## notation.
 axes.formatter.min_exponent: 0      ## minimum exponent to format in scientific notation
# axes.formatter.useoffset: True      ## If True, the tick label formatter
  axes.formatter.useoffset: False      ## If True, the tick label formatter
                                      ## will default to labeling ticks relative
                                      ## to an offset when the data range is
                                      ## small compared to the minimum absolute
                                      ## value of the data.
# axes.formatter.offset_threshold: 6  ## When useoffset is True, the offset
                                      ## will be used when it can remove
                                      ## at least this number of significant
                                      ## digits from tick labels.
                                      #
